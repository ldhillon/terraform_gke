output "gcp" {
  value = {
    us_central1 = {
      spanner_instance_name = module.join_cwow_spanner.env.instance_name
      spanner_project_id    = module.join_cwow_spanner.env.project_id
      bucket_name           = google_storage_bucket.dataflow.name
      project_id            = google_project.apoc.project_id
      spanner_database      = module.spanner_database.name
      workloadid_sa         = google_service_account.apoc_workload_id.email
    }
  }
}
