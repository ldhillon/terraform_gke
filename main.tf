terraform {
  backend "gcs" {}
  required_providers {
    google = "~> 3.4"
    null   = "~> 2.1"
    vault  = "~> 2.8"
  }
  required_version = "> 0.12.20"
}

locals {
  env_id = terraform.workspace == "default" ? "prod" : terraform.workspace
  # Derive the short environment identifier using the terraform workspace
  env_short_id = substr(local.env_id, 0, 6)
}

module "join_cwow" {
  source = "git::https://gitlab.gcp.davita.com/cwow/cwow-program.git?ref=v2.5.2"
  cloud  = "gcp"
  region = "us-central1"
  env    = terraform.workspace == "default" ? "prod" : "np"
}

module "join_cwow_spanner" {
  source = "git::https://gitlab.gcp.davita.com/cwow/spanner.git?ref=v2.0.11"
  cloud  = "gcp"
  region = "us-central1"
  env    = terraform.workspace == "default" ? "p" : "np"
}

resource "random_id" "suffix" {
  byte_length = 2
}

# Apoc app project
resource "google_project" "apoc" {
  name                = "APOC"
  project_id          = "cwow-apoc-${local.env_short_id}-${random_id.suffix.hex}"
  folder_id           = module.join_cwow.env.folder_id
  billing_account     = module.join_cwow.env.billing_account
  auto_create_network = false
  labels = {
    env      = substr(local.env_id, 0, 62)
    dept     = "0844"
    ppsid    = "a0854"
    location = "05555"
  }
}

resource "google_project_iam_member" "apoc_project_iam_member" {
  for_each = toset([
    "roles/cloudprofiler.agent",
    "roles/clouddebugger.agent",
  ])
  project = module.join_gke.env.project_id
  role    = each.value
  member  = "serviceAccount:${google_service_account.apoc_workload_id.email}"
}

# Google APIs for the apoc project
resource "google_project_service" "apoc" {
  for_each = toset([
    "cloudresourcemanager.googleapis.com",
    "storage-api.googleapis.com",
    "storage-component.googleapis.com",
    "cloudbilling.googleapis.com",
    "dataflow.googleapis.com",
    "spanner.googleapis.com",
    "compute.googleapis.com",
    "cloudprofiler.googleapis.com",
    "clouddebugger.googleapis.com",
  ])
  project                    = google_project.apoc.project_id
  service                    = each.value
  disable_on_destroy         = false
  disable_dependent_services = false
}

# App Engine must be enabled to use GCF
resource "google_app_engine_application" "app" {
  project     = google_project.apoc.project_id
  location_id = "us-central"
}

resource "google_compute_shared_vpc_service_project" "apoc" {
  host_project    = module.join_cwow.env.shared_vpc.project_id
  service_project = google_project.apoc.project_id
}

resource "google_compute_subnetwork_iam_member" "apoc_access" {
  provider   = google-beta
  project    = module.join_cwow.env.shared_vpc.project_id
  region     = "us-central1"
  subnetwork = module.join_cwow.env.dataflow.subnetwork
  member     = "serviceAccount:${google_project.apoc.number}@cloudservices.gserviceaccount.com"
  role       = "roles/compute.networkUser"
}

# TODO: Remove this as it's no longer needed, just need to coordinate with team USB
resource "google_storage_bucket" "backups" {
  name               = "apoc-${substr(sha1(terraform.workspace), 0, 4)}-spanner-backup-${random_id.suffix.hex}"
  storage_class      = "REGIONAL"
  location           = "us-central1"
  bucket_policy_only = true
  project            = module.join_cwow_spanner.env.backups.project_id
  force_destroy      = true
}

module "spanner_database" {
  source         = "git::https://gitlab.gcp.davita.com/cwow/spanner.git//modules/spanner-database?ref=v2.0.19"
  project_id     = google_project.apoc.project_id
  name           = "apoc-${local.env_short_id}-${random_id.suffix.hex}"
  spanner        = module.join_cwow_spanner.env
  gcs_backup_uri = "gs://${module.join_cwow.env.backups_bucket}/apoc/spanner/${local.env_id}"
}

resource "google_storage_bucket" "dataflow" {
  name               = "cwow-apoc-dataflow-${local.env_short_id}-${random_id.suffix.hex}"
  storage_class      = "REGIONAL"
  location           = "us-central1"
  bucket_policy_only = "true"
  project            = google_project.apoc.project_id
  force_destroy      = true
}

resource "google_storage_bucket" "apoc_records" {
  name               = "cwow-apoc-records-${local.env_short_id}-${random_id.suffix.hex}"
  storage_class      = "MULTI_REGIONAL"
  bucket_policy_only = "true"
  project            = google_project.apoc.project_id
  force_destroy      = true
  lifecycle_rule {
    condition {
      age = "1095"
    }
    action {
      type          = "SetStorageClass"
      storage_class = "COLDLINE"
    }
  }
}

# Creates a GCP Token for APOC application specific needs.
module "gcp_roleset_apoc_app_sa" {
  source = "git::https://bitbucket.davita.com/scm/vaul/terraform-vault-modules.git//modules/gcp_roleset?ref=v1.4.1"
  metadata = {
    program     = "cwow"
    accessLevel = terraform.workspace == "default" ? "prod" : "non-prod"
    env         = ""
  }
  roleset      = "apoc-sa-${local.env_short_id}-${random_id.suffix.hex}"
  backend      = module.join_cwow.env.vault.gcp_mount.backend
  project      = google_project.apoc.project_id
  secret_type  = "access_token"
  token_scopes = ["https://www.googleapis.com/auth/cloud-platform"]
  bindings = [
    {
      resource = "//spanner.googleapis.com/projects/${module.join_cwow_spanner.env.project_id}/instances/${module.join_cwow_spanner.env.instance_name}/databases/${module.spanner_database.name}"
      roles    = ["organizations/${module.join_cwow.env.org_id}/roles/CwowAppSpannerWriter"]
    },
    {
      resource = "//storage.googleapis.com/buckets/${google_storage_bucket.apoc_records.id}"
      roles    = ["roles/storage.objectAdmin"]
    }
  ]
}

# Creating APOC specific approle
module "approle_role_apoc_app_sa" {
  source  = "git::https://bitbucket.davita.com/scm/vaul/terraform-vault-modules.git//modules/approle_role?ref=v1.4.1"
  approle = "apoc-app-sa-${local.env_short_id}-${random_id.suffix.hex}"
  metadata = {
    program     = "cwow"
    accessLevel = terraform.workspace == "default" ? "prod" : "non-prod"
    env         = ""
  }
  accessor        = module.join_cwow.env.vault.approle_mount.accessor
  approle_backend = module.join_cwow.env.vault.approle_mount.backend
  policies = [
    module.gcp_roleset_apoc_app_sa.app_policy_name
  ]
}

resource "google_spanner_database_iam_member" "apoc_spanner_dev" {
  count    = contains(["dev", "idev"], terraform.workspace) ? 1 : 0
  project  = module.join_cwow_spanner.env.project_id
  instance = module.join_cwow_spanner.env.instance_name
  database = module.spanner_database.name
  role     = "roles/spanner.databaseReader"
  member   = "group:gcp-cwow-apoc-dev@davita.com"
}

resource "google_spanner_database_iam_member" "softserve" {
  count    = terraform.workspace == "dev" ? 1 : 0
  project  = module.join_cwow_spanner.env.project_id
  instance = module.join_cwow_spanner.env.instance_name
  database = module.spanner_database.name
  role     = "organizations/${module.join_cwow.env.org_id}/roles/CwowAppSpannerWriter"
  member   = "group:gcp-softserve-dev@davita.com"
}

resource "google_folder_iam_member" "qa" {
  count  = terraform.workspace == "qa" ? 1 : 0
  folder = module.join_cwow.env.folder_id
  member = "group:gcp-cwow-apoc-qa@davita.com"
  role   = "organizations/${module.join_cwow.env.org_id}/roles/CWOWSpannerQuery"
}

resource "google_spanner_database_iam_member" "apoc_spanner_dev_write" {
  count    = terraform.workspace == "dev" ? 1 : 0
  project  = module.join_cwow_spanner.env.project_id
  instance = module.join_cwow_spanner.env.instance_name
  database = module.spanner_database.name
  role     = "organizations/${module.join_cwow.env.org_id}/roles/CwowAppSpannerWriter"
  member   = "group:gcp-cwow-apoc-dev@davita.com"
}
#####################
# Workload identity #
#####################

module join_gke {
  source = "git::https://gitlab.gcp.davita.com/cwow/gke.git?ref=v1.5.10"
  cloud  = "gcp"
  region = "us-central1"
  env    = terraform.workspace == "default" ? "p" : "np"
}

# data resource to fetch the kubernetes master access token
data "google_client_config" "default" {
  provider = google
}

# specify kubernetes provider for kubernetes resources access
provider "kubernetes" {
  load_config_file       = false
  host                   = "https://${module.join_gke.env.master_endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.join_gke.env.ca_certificate)
}

# Service account for Workload Identity
resource "google_service_account" "apoc_workload_id" {
  account_id   = "apoc-wklid-${substr(sha1(terraform.workspace), 0, 4)}"
  display_name = "cwow apoc worload identity SA"
  project      = google_project.apoc.project_id
}

# Grant workload-id GSA permission to access spanner database 
resource "google_spanner_database_iam_member" "apoc_workload_id_spanner" {
  project  = module.join_cwow_spanner.env.project_id
  instance = module.join_cwow_spanner.env.instance_name
  database = module.spanner_database.name
  role     = "organizations/${module.join_cwow.env.org_id}/roles/CwowAppSpannerWriter"
  member   = "serviceAccount:${google_service_account.apoc_workload_id.email}"
}

# Grant workload-id GSA permission to access storage bucket
resource "google_storage_bucket_iam_member" "apoc_workload_id_storage" {
  bucket = google_storage_bucket.apoc_records.name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${google_service_account.apoc_workload_id.email}"
}

locals {
  # Maps the workspace into a namespace
  namespace_map = {
    default = ["prod-common"]
    dev     = ["dev-common", "dev-sbx-common"]
    idev    = ["idev-common", "idev-poc-common"]
    idev2   = ["idev2-common"]
    uat     = ["uat-common"]
    qa      = ["qa-common"]
    qa2     = ["qa2-common"]
    stage   = ["stage-common"]
    dyn_env = ["default"] # default means namespace here, not workspace
  }
  namespaces = lookup(local.namespace_map, terraform.workspace, local.namespace_map.dyn_env)
}

# Creates domain level kubernetes service account
resource "kubernetes_service_account" "apoc_ksa" {
  for_each = toset(local.namespaces)
  metadata {
    name      = "apoc"
    namespace = each.value
    annotations = {
      "iam.gke.io/gcp-service-account" = google_service_account.apoc_workload_id.email
    }
    labels = {
      "domain" = "apoc"
    }
  }
  automount_service_account_token = true
}

# GSA to KSA Mapping for the apoc domain
resource "google_service_account_iam_member" "apoc_gsa_ksa_map" {
  for_each           = toset(local.namespaces)
  service_account_id = google_service_account.apoc_workload_id.name
  role               = "roles/iam.workloadIdentityUser"
  member             = "serviceAccount:${module.join_gke.env.workloadid_namespace}[${each.value}/apoc]"
}


# GSA to KSA Mapping for spanner access
resource "google_service_account_iam_member" "apoc_search_spanner_wklid" {
  for_each           = toset(local.namespaces)
  service_account_id = google_service_account.apoc_workload_id.name
  role               = "roles/iam.workloadIdentityUser"
  member             = "serviceAccount:${module.join_gke.env.workloadid_namespace}[${each.value}/cwow-apoc-search-spanner]"
}

# GSA to KSA Mapping for spanner access
resource "google_service_account_iam_member" "apoc_gcp_spanner_wklid" {
  for_each           = toset(local.namespaces)
  service_account_id = google_service_account.apoc_workload_id.name
  role               = "roles/iam.workloadIdentityUser"
  member             = "serviceAccount:${module.join_gke.env.workloadid_namespace}[${each.value}/cwow-apoc-gcp-spanner]"
}

# GSA to KSA Mapping for spanner access
resource "google_service_account_iam_member" "apoc_batch_gcp_spanner_wklid" {
  for_each           = toset(local.namespaces)
  service_account_id = google_service_account.apoc_workload_id.name
  role               = "roles/iam.workloadIdentityUser"
  member             = "serviceAccount:${module.join_gke.env.workloadid_namespace}[${each.value}/cwow-apoc-batch-gcp-spanner]"
}
