#!/bin/sh
################################################################################
# Semantically versions a terraform module and generates a changelog           #
#                                                                              #
# Author: Stephen Roberts <stephen.roberts1@davita.com>                        #
#                                                                              #
# WARNING: This script is designed to work with GitLab -- use otherwise at     #
#          your own peril!                                                     #
#                                                                              #
# Prerequisites                                                                #
#   curl                                                                       #
#   git                                                                        #
#   jq                    - https://stedolan.github.io/jq/                     #
#   terraform             - https://www.terraform.io                           #
#   terraform-json-schema - https://bitbucket.davita.com/projects/SDLC/repos/  #
#                           terraform-json-schema/browse                       #
#   auto-semver           - https://gitlab.com/love.wearensr/auto-semver       #
#                                                                              #
# Globals                                                                      #
#   CI_REPOSITORY_URL   - the http url to the git repo                         #
#   CI_COMMIT_REF_NAME  - the current git branch                               #
#   CI_PROJECT_ID       - id of the project running the job                    #
#   MR_ROBOT_TOKEN      - access token for Mr. Robot                           #
#                                                                              #
# Usage: terraform-semver.sh [relative path to the root module]                #
#                                                                              #
# Overview                                                                     #
#   - Initializes a new repo with an initial release tag and changelog         #
#   - Shallow clones the current terraform module release                      #
#   - Diffs the current release against the new terraform module               #
#   - Generates the next semantic version based on the changes                 #
#   - Generates a changelog of the changes                                     #
#   - For feature branches, pushes the partial changelog into the              #
#     corresponding merge request                                              #
#   - For master, updates the changelog, commits and pushes the changes to the #
#     repo, and tags the new release with the semantic version                 #
################################################################################

set -e

###########################################
# Gets the latest git tag from the remote #
#                                         #
# Return: git tag                         #
###########################################
latest_tag() {
  git fetch --tags >/dev/null
  # Use git's version sort to get the most recent version tag
  git tag -l --sort=v:refname | tail -1
}

##########################################
# Clones and configures the last release #
#                                        #
# Arguments                              #
#   - repo url                           #
#   - branch/tag                         #
#   - target directory for git clone     #
##########################################
init_last_release() {
  # Shallow clone the last tagged release
  printf -- "==> Cloning %s branch into %s...\n" "$2" "$3"
  git clone --depth 1 "$1" -b "$2" "$3"

  # Terraform init disappears to be working when you specifiy a directory at the
  # end of the command, so we gotsta change the working directory ¯\_(ツ)_/¯
  (cd "$3" && terraform init -input=false)
}

##################################################################
# Generates an initial json schema for both "a" and "b" versions #
#                                                                #
# Arguments                                                      #
#   - path for schema a                                          #
#   - path for schema b                                          #
##################################################################
init_schemas() {
  cat | tee "$1" > "$2" <<EOF
{
  "\$schema": "http://json-schema.org/draft-07/schema#",
  "type": "object",
  "properties": {
    "variables": {
      "type": "object"
    },
    "outputs": {
      "type": "object",
      "properties": {
        "gcp": {
          "type": "object",
          "properties": {
            "us-central1": {
              "type": "object"
            }
          }
        }
      }
    }
  },
  "required": [
    "variables",
    "outputs"
  ]
}
EOF
}

################################################################################
# Generates a json schema from a terraform module by running "terraform apply" #
# to get the state file for this set of inputs then extracting the schema and  #
# merging into the composite schema of all inputs variations.                  #
#                                                                              #
# Arguments                                                                    #
#   - path to the terraform module                                             #
#   - path for schema                                                          #
#   - cloud                                                                    #
#   - region                                                                   #
#   - workspace                                                                #
################################################################################
generate_schema() {
  printf -- "==> Generating json schema for %s...\n" "$1"
  
  # Apply to generate the state file for this combination of inputs
  (cd "$1"
  terraform apply \
    -var cloud="$3" \
    -var region="$4" \
    -var workspace="$5")

  # Generate the schema for this combination of inputs
  terraform-json-schema "$1" > "$2.schema"

  # Merge into the composite schema
  jq \
    -rs \
    --arg path "properties,outputs,properties,$3,properties,$4" \
    '.[1].properties.variables as $vars
    | .[1].properties.outputs as $outputs
    | .[0]
    | setpath($path | split(","); $outputs)
    | setpath(["properties","variables"]; $vars)' \
    "$2" "$2.schema" > "$2.tmp"
  mv "$2.tmp" "$2"
  rm "$2.schema"
}

#############################################################################
# Generates a changelog entry (markdown format) from the autosemver results #
#                                                                           #
# Arguments                                                                 #
#   - autosemver results                                                    #
#############################################################################
generate_changelog() {
  if test "$(printf "%s" "$1" | jq -r .changeset)" = "null"; then
    echo "## ${next_version} - $(date "+%Y-%m-%d")
- Internal changes
"
  else
    printf "%s" "$1" | jq -r --arg date "$(date "+%Y-%m-%d")" \
      '.version as $version
      | { changeset: [] } + . | .changeset
      | map({
          verb: ["Changed", "Changed", "Changed", "Added", "Removed"][.type-1],
          type: .path | split(".")[1] | sub("s$"; ""),
          field: .path | split(".")[2:] | join("."),
          from: (
            [
              if .typeA != null and .typeB != null then
                .typeA
              else
                null
              end,

              if .type == 2 then
                "optional"
              elif .type == 3 then
                "required"
              else
                null
              end
            ]
            | map(if . == null then "" else "from " + . end)
          ) | join(""),
          to: (
            [
              if .typeA != null and .typeB != null then
                .typeB
              else
                null
              end,

              if .type == 2 then
                "required"
              elif .type == 3 then
                "optional"
              else
                null
              end
            ] | map(if . == null then "" else "to " + . end)
          ) | join(""),
        })
      | map(
          "- " + .verb + " " + .type + " " + .field + " " + .from + " " + .to
          | sub(" +$"; "")
        )
      | join("\n")
      | "## v" + $version + " - " + $date + "\n" + . + "\n"'
  fi
}

# Initialize variables and defaults
path="$1"
schema_a=a.json
schema_b=b.json
tag=$(latest_tag)
next_version=v1.0.0
changelog="## ${next_version} - $(date "+%Y-%m-%d")
- Initial commit
"
semver_dir=.terraform-semver
mkdir -p "$semver_dir"
terraform init -input=false

if test -z "$tag"; then
  printf -- "==> Initial version: v1.0.0"
else
  init_last_release "$CI_REPOSITORY_URL" "$tag" "$semver_dir/$tag"
  init_schemas "$schema_a" "$schema_b"

  # shellcheck disable=SC2043
  for cloud in gcp; do
    for region in us-central1; do
      # Generate the json schema for version "a"
      generate_schema "$semver_dir/$tag" "$schema_a" "$cloud" "$region" "$tag"

      # Generate the json schema for version "b"
      generate_schema "./" "$schema_b" "$cloud" "$region" "$TF_WORKSPACE"
    done
  done

  printf -- "==> Generating the module changeset...\n"
  results="$(auto-semver \
    -current-schema "$schema_a" \
    -new-schema "$schema_b" \
    -ruleset config/semver.yaml \
    -current-version "${tag#v}"
  )"

  # Extract the next version number from the results
  next_version="v$(printf -- "%s" "$results" | jq -r .version)"

  printf -- "==> Current version: %s\n" "$tag"
  printf -- "==> Next version: %s\n" "$next_version"

  printf -- "==> Generating a changelog...\n"
  changelog=$(generate_changelog "$results")
fi

printf -- "\n\n%b\n\n" "$changelog"

# Push changelog to merge request
HEADER="## Changelog" \
MESSAGE="$changelog" \
./scripts/gitlab-mr.sh

if test "$CI_COMMIT_REF_NAME" == "master"; then
  new_origin=$(echo "$CI_REPOSITORY_URL" \
    | sed "s/gitlab-ci-token:.*@/mr-robot:$MR_ROBOT_TOKEN@/")
  git remote set-url origin "$new_origin"
  git config user.name "Mr. Robot"
  git config user.email "mrrobot@davita.com"

  # Fetch latest changes
  git fetch origin master

  # Hard reset to master
  git reset --hard origin/master

  # Generate changelog changes
  echo "$changelog" > .changes.md
  ./scripts/changelog-update.sh .changes.md

  # Commit and push the changelog
  git add CHANGELOG.md
  git commit -m "docs(changelog): add ${next_version} [skip ci]"
  git push origin HEAD:master

  # Tag and push
  git tag "$next_version"
  git push origin "$next_version"

  # Push the remote state to a new workspace corresponding to the git tag
  (cd "$path"
  TF_WORKSPACE=default terraform state pull > terraform.tfstate
  TF_WORKSPACE="$next_version" terraform workspace new "$next_version"
  TF_WORKSPACE="$next_version" terraform state push terraform.tfstate)

  release_body=$(jq -rn \
    --arg version "$next_version" \
    --arg changelog "$changelog" \
    '{
    name: ("Release " + $version),
    tag_name: $version,
    description: $changelog,
  } | tojson')

  # Create a release
  release=$(curl --fail \
    --show-error \
    --silent \
    --header 'Content-Type: application/json' \
    --header "Authorization: Bearer $MR_ROBOT_TOKEN" \
    --request POST \
    --data "$release_body" \
    "$CI_API_V4_URL/projects/${CI_PROJECT_ID}/releases" 2>&1) \
    || { printf -- "Failed to create a release!\n\n%b\n" "$release"; exit 1; }
fi
