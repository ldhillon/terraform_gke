#!/bin/sh
##############################################################################
# Commits the latest auto-upgrade version to the repo                        #
#                                                                            #
# WARNING: This script is designed  exclusively to work with GitLab CI!      #
#                                                                            #
# Author: Stephen Roberts <stephen.roberts1@davita.com>                      #
#                                                                            #
# GLOBALS                                                                    #
#                                                                            #
#   Name                Req   Description                                    #
#   TOKEN               Yes   GitLab token for repo and api                  #
#   TOOL                Yes   Tool name for string replacement when updating #
#                             version                                        #
#   VERSION             Yes   Version number                                 #
#   COMMIT_MSG          No    Git commit message                             #
#   TAG                 No    Git tag to push (default: no tag is pushed)    #
#   RELEASE             No    Create a GitLab release (default: false)       #
#   RELEASE_NAME        No    (default: Release $TAG)                        #
#   RELEASE_DESCRIPTION No    (default: - $TOOL $TAG)                        #
##############################################################################

# Required variables
: "${TOKEN:?}"
: "${TOOL:?}"
: "${VERSION:?}"

# Optional variables
: "${COMMIT_MSG:="feat(pipeline): auto-upgrade $TOOL to $VERSION [skip ci]"}"
: "${RELEASE:=false}"
: "${RELEASE_NAME:="Release $TAG"}"
: "${RELEASE_DESCRIPTION:="- $TOOL $TAG"}"

if [ "$RELEASE" = true ] && [ -z "$TAG" ]; then
  echo "Creating a release requires a git tag. Set the TAG variable."
  exit 1
fi

set -e

apk --update --no-cache add curl git

# Configure git for committing
new_origin=$(echo "$CI_REPOSITORY_URL" \
  | sed "s/gitlab-ci-token:.*@/mr-robot:$TOKEN@/")
git remote set-url origin "$new_origin"
git config user.name "Mr. Robot"
git config user.email "mrrobot@davita.com"
git fetch origin "$CI_COMMIT_REF_NAME"
git reset --hard "origin/$CI_COMMIT_REF_NAME"

# Update version-lock in pipeline
sed -i'' 's/\(^[[:space:]]*'"$TOOL"':[[:space:]]*\).*$/\1'"$VERSION"'/' .gitlab-ci.yml

# Commit and push
git add .gitlab-ci.yml
git commit -m "$COMMIT_MSG"
git push origin "HEAD:$CI_COMMIT_REF_NAME"

# Tag and push
if [ -n "$TAG" ]; then
  git tag "$TAG"
  git push origin "$TAG"

  # Create a release
  if [ "$RELEASE" = true ]; then
    curl --fail \
      --show-error \
      --silent \
      --header "Authorization: Bearer $TOKEN" \
      --request POST \
      --form "name=$RELEASE_NAME" \
      --form "tag_name=$TAG" \
      --form "description=$RELEASE_DESCRIPTION" \
      "$CI_API_V4_URL/projects/$CI_PROJECT_ID/releases" || {
        echo "Failed to create a release!"
        exit 1
      }
  fi
fi
