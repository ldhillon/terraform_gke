#!/bin/sh

set -e

: "${VAULT_ADDR:="https://platform-vault.davita.com"}"
: "${VAULT_APPROLE_PATH:="approle/login"}"
: "${VAULT_TOKEN_PATH:=.vault_token}"
: "${VAULT_ROLE_ID:?}"
: "${VAULT_SECRET_ID:?}"
: "${VAULT_GCP_PATH:?}"

echo "==> Fetching vault token"
RESPONSE=$(curl \
  --silent \
  --show-error \
  --fail \
  --request POST \
  --data "{\"role_id\": \"${VAULT_ROLE_ID}\",\"secret_id\": \"${VAULT_SECRET_ID}\"}" \
  "${VAULT_ADDR}/v1/auth/${VAULT_APPROLE_PATH}")
VAULT_TOKEN=$(echo "$RESPONSE" | jq -r '.auth.client_token')
export VAULT_TOKEN

echo "==> Fetching Google OAuth access token"
VAULT_RESULT=$(curl \
  --silent \
  --show-error \
  --fail \
  --header "X-Vault-Token: ${VAULT_TOKEN}" \
  "${VAULT_ADDR}/v1/${VAULT_GCP_PATH}")

GOOGLE_OAUTH_ACCESS_TOKEN=$(echo "$VAULT_RESULT" | jq -r ".data.token")
export GOOGLE_OAUTH_ACCESS_TOKEN
