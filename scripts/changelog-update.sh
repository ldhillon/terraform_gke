#!/bin/sh
##################################################################################
# Updates/creates a changelog with a new entry                                   #
#                                                                                #
# Author: Stephen Roberts <stephen.roberts1@davita.com>                          #
#                                                                                #
# Usage: changelog-update [OPTIONS] <FILE>                                       #
#                                                                                #
#   Inserts the contents of FILE at the top of the changelog entries. The format #
#   is expected to adhere to keepachangelog.com and the first changelog entry is #
#   found by scanning for "##" in the file unless an explicit offset is given.   #
#                                                                                #
# Options                                                                        #
#   -l, --line        The line number at which to insert the changelog entry. By #
#                     default, the script will scan for the first occurrence of  #
#                     "##" in the file and insert the entry at that line.        #
#   -c, --changelog   The name/path of the CHANGELOG to update. Default to       #
#                     CHANGELOG.md                                               #
##################################################################################

set -e

while test $# -gt 0; do
  case "$1" in
    --line|-l)
      line="$2"
      shift; shift;;
    --changelog|-c)
      changelog="$2"
      shift; shift;;
    *)
      file="$1"
      shift;;
  esac
done

: "${file?File to insert into the changelog is required!}"
: "${changelog:=CHANGELOG.md}"

# Create a changelog if one doesn't exist
if test ! -f "$changelog"; then
  printf -- "# Changelog\n\nThis changelog is auto-generated. Please do not \
modify!\n\n" > "$changelog"
fi

if test -z "$line"; then
  # Scan for "##" in the changelog if the line number wasn't provided
  line_end=$(awk '/##/{print NR}' "$changelog" | head -n 1)

  # Append to the end of the file if "##" wasn't found
  if test -z "$line_end"; then
    line_end=$(wc -l "$changelog" | awk '{print $1}')
    line_end=$((line_end+1))
  fi
else
  line_end="$line"
fi
line_start=$((line_end-1))

# Copy changelog header into a new temp file
head "-$line_start" "$changelog" > .CHANGELOG.md

# Append FILE contents followed by newline
{ cat "$file"; printf -- "\n"; } >> .CHANGELOG.md

# Append the rest of the changelog
tail -n "+$line_end" "$changelog" >> .CHANGELOG.md

# Replace the old changelog with the new one
mv .CHANGELOG.md "$changelog"
